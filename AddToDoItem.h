//
//  AddToDoItem.h
//  ToDoList
//
//  Created by Irene Lee on 7/17/14.
//
//

#import <UIKit/UIKit.h>
#import "ToDoObject.h"

@interface AddToDoItem : UIViewController

@property ToDoObject *toDoItem;

@end
