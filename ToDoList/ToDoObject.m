//
//  ToDoObject.m
//  ToDoList
//
//  Created by Irene Lee on 7/17/14.
//
//

#import "ToDoObject.h"

@implementation ToDoObject
- (id)init
{
    self = [super init];
    if (self)
    {
        _completed = NO;
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self)
    {
        _itemName = [coder decodeObjectForKey:@"itemName"];
        _completed = [coder decodeBoolForKey:@"completed"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.itemName forKey:@"itemName"];
    [coder encodeBool:self.completed forKey:@"completed"];
}

@end
