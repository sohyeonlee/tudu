//
//  ToDoObject.h
//  ToDoList
//
//  Created by Irene Lee on 7/17/14.
//
//

#import <Foundation/Foundation.h>

@interface ToDoObject : NSObject

@property NSString *itemName;
@property BOOL      completed;

@end
